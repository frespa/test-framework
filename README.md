# Next Generation Testing framework

This testing framework is made after having the benefit of using and developing 3 different testing frameworks

I have deliberately made an effort to keep this framework simple and not tied it into any specific test runner

The framework is supposed to be a place that exposes devices and their functionality while also exposing the functionally of probing hardware in a simple way. The user should not need to focus on the inner workings of devices nor probing hardware.

## Framework sections

### Devices

A device can be a DUT (Device Under Test) or a device that interacts with the DUT.

### Probes

This is something that is not considered a product, but non the less is something that exposes functionality to verify the DUT.