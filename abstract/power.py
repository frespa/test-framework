"""
Power related functionality exposed by a device
"""

from abc import ABC, abstractmethod

class Power(ABC):
    """
    This is an abstract class describing power related functionality exposed by a device
    """

    @abstractmethod
    def isPoweredOn(self) -> bool:
        """
        Returns:
            True if the device is powered on, otherwise False
        """
        return NotImplemented

    @abstractmethod
    def powerOn(self) -> None:
        """
        Changes the power state of the device to "on"
        """
        return NotImplemented

    @abstractmethod
    def powerOff(self) -> None:
        """
        Changes the power state of the device to "off"
        """
        return NotImplemented