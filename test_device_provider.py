from unittest import TestCase
from devices.dummy import Dummy

from device_provider import DeviceProvider


class DeviceProviderTest(TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.provider = DeviceProvider()

    def testProvider(self):
        dummy = self.provider.getDeviceOfType(Dummy)

        self.assertIsInstance(dummy, Dummy)

        self.assertFalse(dummy.isPoweredOn())
        dummy.powerOn()
        self.assertTrue(dummy.isPoweredOn())



