"""
Verifies the dummy class
"""

from unittest import TestCase

from devices.dummy import Dummy

class TestDummy(TestCase):
    """
    Verifies the basic functionality that Dummy exposes
    """

    def testIsPoweredOn(self):

        dut = Dummy()

        self.assertFalse(dut.isPoweredOn())

    def testTogglePowerState(self):
        """
        Toggles the power state of the device and verifies the result
        """

        dut = Dummy()

        self.assertFalse(dut.isPoweredOn())

        dut.powerOn()

        self.assertTrue(dut.isPoweredOn())

        dut.powerOff()
        self.assertFalse(dut.isPoweredOn())
