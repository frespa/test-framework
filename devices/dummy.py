
import abstract.power

class Dummy(abstract.power.Power):
    """
    A basic device
    """

    def __init__(self):
        super().__init__()
        self._poweredOn = False

    def isPoweredOn(self):
        return self._poweredOn

    def powerOff(self):
        self._poweredOn = False

    def powerOn(self):
        self._poweredOn = True
