from rpyc.utils.zerodeploy import DeployedServer
from plumbum import SshMachine

# create the deployment
mach = SshMachine("172.17.0.2", user="root", password="THEPASSWORDYOUCREATED")
server = DeployedServer(mach)

# and now you can connect to it the usual way
conn1 = server.classic_connect()
print(conn1.modules.sys.platform)

# you're not limited to a single connection, of course
conn2 = server.classic_connect()
print(conn2.modules.os.getpid())

# when you're done - close the server and everything will disappear
server.close()